import {
  Component,
  ViewChild,
  ElementRef,
  OnInit,
  Input,
  NgZone,
} from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import {
  Geolocation,
  PushNotification,
  PushNotificationToken,
  PushNotificationActionPerformed,
  Plugins,
  CameraDirection,
} from "@capacitor/core";
import { Platform } from "@ionic/angular";
import {
  ILatLng,
  LatLng,
  CameraPosition,
  GoogleMap,
  GoogleMaps,
  GoogleMapsEvent,
  MyLocation,
  GoogleMapsAnimation,
  Marker,
  Polygon,
  Circle,
  Polyline,
} from "@ionic-native/google-maps/ngx";
import {
  NativeGeocoder,
  NativeGeocoderOptions,
  NativeGeocoderResult,
} from "@ionic-native/native-geocoder/ngx";
import { from } from "rxjs";
// import { GoogleMap, GoogleMaps, GoogleMapsOptions } from '@ionic-native/google-maps
// import { map } from 'rxjs/operators';
// import { GoogleMaps, GoogleMap, Environment, Marker } from '@ionic-native/google-maps/ngx';

const { PushNotifications, Modals } = Plugins;
declare var google;

@Component({
  selector: "app-home",
  templateUrl: "home.page.html",
  styleUrls: ["home.page.scss"],
})
export class HomePage implements OnInit {
  lat: any;
  long: any;
  @ViewChild("mapElement") mapElement: ElementRef;
  public search: string = "";
  private googleAutoComplete = new google.maps.places.AutocompleteService();
  public searchReasults = new Array<any>();
  map: GoogleMap;
  markers = [];
  autocomplete: any = {};
  circleData: Circle;
  fence: number = 200;
  storedLatLng: any = [];
  latlat: number;
  lnglng: number;
  collectPoly: any;
  polyineArray: any[] = [];
  keepMark: any[] = [];
  liveMark: any;

  userForm = new FormGroup({
    name: new FormControl("Akash", [
      Validators.required,
      Validators.minLength(5),
      Validators.maxLength(20),
    ]),
    email: new FormControl("", Validators.required),
    address: new FormGroup({
      password: new FormControl(),
      city: new FormControl(),
      postalCode: new FormControl(null, Validators.pattern("^[1-9][0-9]{4}$")),
    }),
  });
  newLat: any;
  newLng: any;

  onclick() {
    console.log(this.userForm.value);
  }
  constructor(
    public platform: Platform,
    private nativeGeocoder: NativeGeocoder,
    private ngZone: NgZone
  ) {
    // registerWebPlugin()
    console.log(google);
    console.log(this.map);
    this.searchChanged();
    console.log(platform.height());
    console.log(platform.width());
    //this.watchPosition();
  }

  async ngOnInit() {
    await this.platform.ready();
    //this.loadMap();
    this.getLocation();
    this.drawGeofence();
    this.autocomplete = {
      query: "",
      creation_type: "circular",
    };
    // // Register with Apple / Google to receive push via APNS/FCM
    // PushNotifications.register();

    // // On succcess, we should be able to receive notifications
    // PushNotifications.addListener('registration',
    //   (token: PushNotificationToken) => {
    //     alert('Push registration success, token: ' + token.value);
    //     console.log('Push registration success, token: ' + token.value);
    //   }
    // );

    // // Some issue with our setup and push will not work
    // PushNotifications.addListener('registrationError',
    //   (error: any) => {
    //     alert('Error on registration: ' + JSON.stringify(error));
    //   }
    // );

    // // Show us the notification payload if the app is open on our device
    // PushNotifications.addListener('pushNotificationReceived',
    //   (notification: PushNotification) => {
    //     var audio1 = new Audio('assets/ignitionon.mp3');
    //     console.log('Audio');
    //     audio1.play();
    //     // alert('Push received: ' + JSON.stringify(notification));
    //     console.log('Push received: ', notification);

    //     let alertRet = Modals.alert({
    //       title: notification.title,
    //       message: notification.body
    //     });

    //   }
    // );

    // // Method called when tapping on a notification
    // PushNotifications.addListener('pushNotificationActionPerformed',
    //   (notification: PushNotificationActionPerformed) => {
    //     alert('Push action performed: ' + JSON.stringify(notification));
    //     console.log('Push action performed: ' + notification);
    //   }
    // );
  }

  // loadMap() {
  //   const that = this;
  //   const latLng = new google.maps.LatLng(this.lat, this.long);
  //   const mapOptions = {
  //     center: {lat: this.lat, long: this.long},
  //     zoom: 15,
  //     mapTypeId: google.maps.MapTypeId.ROADMAP
  //   };
  //   this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
  //   let marker: Marker = this.map.addMarkerSync({
  //     title: 'success',
  //     icon: 'blue',
  //     animation: 'DROP',
  //     position: latLng
  //   });
  //   marker.showInfoWindow();
  // }

  async getLocation() {
    const position = await Geolocation.getCurrentPosition();
    console.log("details", position);
    this.lat = position.coords.latitude;
    this.long = position.coords.longitude;
    console.log(this.lat);
    const latLng = new google.maps.LatLng(this.lat, this.long);
    const mapOptions = {
      center: latLng,
      zoom: 15,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      disableDefaultUI: true,
    };
    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
    let marker = new google.maps.Marker({
      map: this.map,
      animation: google.maps.Animation.DROP,
      position: {
        lat: this.lat,
        lng: this.long,
      },
    });
    //marker.showInfoWindow();
    console.log("map", this.map.moveCamera);
  }

  watchPosition() {
    const wait = Geolocation.watchPosition({}, (position, err) => {});
    console.log("detalis 2", wait);
  }

  async addOriginalMarker() {
    try {
      const myLocation: MyLocation = await this.map.getMyLocation();
      console.log("getmylocation", myLocation);
    } catch (error) {
      console.log(error);
    }
  }

  searchChanged() {
    //console.log("what you type", this.search);
    if (!this.search.trim().length) return;
    this.googleAutoComplete.getPlacePredictions(
      { input: this.search },
      (data) => {
        this.ngZone.run(() => {
          this.searchReasults = data;
        });
        //console.log("cheack", data);
      }
    );
    console.log("result", this.searchReasults);
  }


  showPlace(item: any) {
    debugger;
    console.log(item);
    console.log("map2", this.map);
    let that = this;
    that.googleAutoComplete.query = item.description;
    console.log("console item=>" + JSON.stringify(item));
    let options: NativeGeocoderOptions = {
      useLocale: true,
      maxResults: 5,
    };
    this.nativeGeocoder
      .forwardGeocode(item.description, options)
      .then((result: NativeGeocoderResult[]) => {
        console.log(
          "The coordinates are latitude=" +
            result[0].latitude +
            " and longitude=" +
            result[0].longitude
        );
        that.newLat = parseFloat(result[0].latitude);
        that.newLng = parseFloat(result[0].longitude);
        debugger;
        let pos: CameraPosition<ILatLng> = {
          target: new LatLng(that.newLat, that.newLng),
          zoom: 15,
          tilt: 30,
        };
        // this.map.animateCamera(pos);
        // that.map.animateCamera({
        //   target: { lat: that.newLat, lng: that.newLng },
        //   zoom: 15,
        //   duration: 3000,
        //   padding: 0 // default = 20px
        //   });

        //   let marker: Marker = this.map.addMarkerSync({
        //   title: 'success',
        //   icon: 'blue',
        //   animation: 'DROP',
        //   position: {
        //     lat: that.newLat,
        //     lng: that.newLng
        //   }
        // });
        this.map.animateCamera(pos);
        let marker = new google.maps.Marker({
          map: this.map,
          animation: google.maps.Animation.DROP,
          position: {
            lat: that.newLat,
            lng: that.newLng,
          },
        });
        //marker.showInfoWindow();
      });
    // const info: any = await this.nativeGeocoder.forwardGeocode{{address: item.description}}
    // this.nativeGeocoder.forwardGeocode(item.description, options)
    // .then((coordinates: NativeGeocoderResult[]) => {
    //   console.log('The coordinates are latitude=' + coordinates[0].latitude + ' and longitude=' + coordinates[0].longitude);
    // });
  }
  callThis(fence) {
    let that = this;
    that.circleData.setRadius(fence);
  }

  radioChecked(key) {
    let that = this;
    console.log("radio btn checked: ", key);
    that.autocomplete.creation_type = key;
    if (key == "polygon") {
      if (that.circleData) {
        that.circleData.remove();
        that.fence = 200;
      }
      that.storedLatLng = [];
    } else {
      if (key == "circular") {
        Geolocation.getCurrentPosition().then((resp) => {
          this.latlat = resp.coords.latitude;
          this.lnglng = resp.coords.longitude;
          let pos: CameraPosition<ILatLng> = {
            target: new LatLng(resp.coords.latitude, resp.coords.longitude),
            zoom: 16,
            tilt: 30,
          };
          this.map.moveCamera(pos);
          if (that.circleData != undefined) {
            that.circleData.remove();
          }

          let ltln: ILatLng = { lat: this.latlat, lng: this.lnglng };
          let circle: Circle = this.map.addCircleSync({
            center: ltln,
            radius: that.fence,
            strokeColor: "#d80622",
            strokeWidth: 2,
            fillColor: "rgb(255, 128, 128, 0.5)",
          });

          that.circleData = circle;
          console.log("circle data: => " + that.circleData);
        });
        if (that.collectPoly) {
          that.collectPoly.remove();
        }
        if (that.polyineArray.length > 0) {
          for (var i = 0; i < that.polyineArray.length; i++) {
            that.polyineArray[i].remove();
          }
        }

        if (that.keepMark.length > 0) {
          for (var r = 0; r < that.keepMark.length; r++) {
            that.keepMark[r].remove();
          }
        }
        that.storedLatLng = [];
      }
    }
  }

  drawGeofence() {
    if (this.map != undefined) {
      this.map.remove();
    }
    this.map = GoogleMaps.create(this.mapElement);
    // Wait the MAP_READY before using any methods.

    this.map.one(GoogleMapsEvent.MAP_READY)
      .then(() => {
        console.log('Map is ready!');
        let that = this;
        if (that.liveMark) {
          that.liveMark.remove();
        }
        Geolocation.getCurrentPosition().then((resp) => {
          this.latlat = resp.coords.latitude;
          this.lnglng = resp.coords.longitude
          let pos: CameraPosition<ILatLng> = {
            target: new LatLng(resp.coords.latitude, resp.coords.longitude),
            zoom: 16,
            tilt: 30
          };
          this.map.moveCamera(pos);
          this.map.addMarker({
            title: '',
            position: new LatLng(resp.coords.latitude, resp.coords.longitude),
          }).then((marker: Marker) => {
            console.log("Marker added")
            that.liveMark = marker;
          })

          if (that.autocomplete.creation_type == 'circular') {
            if (that.circleData != undefined) {
              that.circleData.remove();
            }
            // that.storedLatLng.push(data[0]);
            // Add circle
            let ltln: ILatLng = { "lat": resp.coords.latitude, "lng": resp.coords.longitude };
            let circle: Circle = this.map.addCircleSync({
              'center': ltln,
              'radius': that.fence,
              'strokeColor': '#d80622',
              'strokeWidth': 2,
              'fillColor': 'rgb(255, 128, 128, 0.5)'
            });

            that.circleData = circle;
            console.log("circle data: => " + that.circleData)

          }
        });

        that.map.on(GoogleMapsEvent.MAP_CLICK).subscribe(
          (data) => {
            if (that.autocomplete.creation_type == 'circular') {
              if (that.circleData != undefined) {
                that.circleData.remove();
              }
              // that.storedLatLng.push(data[0]);
              // Add circle
              let ltln: ILatLng = { "lat": data[0].lat, "lng": data[0].lng };
              let circle: Circle = this.map.addCircleSync({
                'center': ltln,
                'radius': that.fence,
                'strokeColor': '#d80622',
                'strokeWidth': 2,
                'fillColor': 'rgb(255, 128, 128, 0.5)'
              });

              that.circleData = circle;
              console.log("circle data: => " + that.circleData)
              // this.map.moveCamera({
              //   target: circle.getBounds()
              // });
            } else {
              if (that.autocomplete.creation_type == 'polygon') {
                var markicon;
                if (that.platform.is('ios')) {
                  markicon = 'www/assets/imgs/circle1.png';
                } else if (that.platform.is('android')) {
                  markicon = './assets/imgs/circle1.png';
                }
                that.storedLatLng.push(data[0]);
                that.map.addMarker({
                  position: data[0],
                  icon: markicon
                }).then((mark: Marker) => {
                  that.keepMark.push(mark);
                  mark.on(GoogleMapsEvent.MARKER_CLICK).subscribe((latLng: LatLng) => {
                    that.storedLatLng.push(that.storedLatLng[0])  // store first lat lng at last also
                    let GORYOKAKU_POINTS: ILatLng[] = that.storedLatLng;
                    that.map.addPolygon({
                      'points': GORYOKAKU_POINTS,
                      'strokeColor': '#000',
                      'fillColor': '#ffff00',
                      'strokeWidth': 5
                    }).then((polygon: Polygon) => {
                      // console.log("GORYOKAKU_POINTS=> " + JSON.stringify(GORYOKAKU_POINTS));
                      that.collectPoly = polygon;
                    });
                  });
                });
                debugger
                // console.log(JSON.stringify(that.storedLatLng));
                if (that.storedLatLng.length > 1) {
                  that.map.addPolyline({
                    points: that.storedLatLng,
                    color: '#000000',
                    width: 3,
                    geodesic: true
                  }).then((poly: Polyline) => {
                    that.polyineArray.push(poly);
                  });
                }
              }
            }
          }
        );
      });
  }
}
