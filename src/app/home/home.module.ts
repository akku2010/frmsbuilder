import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { HomePage } from './home.page';
// import { AgmCoreModule } from '@agm/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    RouterModule.forChild([
      {
        path: '',
        component: HomePage
      }
    ]),
    // AgmCoreModule.forRoot({
    //   apiKey: 'AIzaSyCmSXnoBBGekErvqBvu5N7dbeqdH1pHp7c',
    //   libraries: ['places']
    // }),
  ],
  declarations: [HomePage]
})
export class HomePageModule {}
